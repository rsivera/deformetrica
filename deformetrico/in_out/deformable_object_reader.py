import logging
import warnings
import os


import numpy as np


from ..core.observations.deformable_objects.image import Image
from ..core.observations.deformable_objects.landmarks.landmark import Landmark
from ..core.observations.deformable_objects.landmarks.point_cloud import PointCloud
from ..core.observations.deformable_objects.landmarks.poly_line import PolyLine
from ..core.observations.deformable_objects.landmarks.surface_mesh import SurfaceMesh
from ..core.observations.deformable_objects.landmarks.surface_slice import SurfaceSlice

from ..in_out.image_reader_and_writer import read_image
from ..in_out.image_functions import normalize_image_intensities

#logging.getLogger('PIL').setLevel(logging.WARNING)

class DeformableObjectReader:
    """
    Creates PyDeformetrica objects from specified filename and object type.
    """
    @staticmethod
    def create_object(object_filename, object_type, dimension=None):

        if object_type.lower() == 'SurfaceMesh'.lower():
            out_object = SurfaceMesh()
            out_object.read(object_filename)
            out_object.remove_null_normals()

        elif object_type.lower() == 'SurfaceSlice'.lower():
            out_object = SurfaceSlice()
            out_object.read(object_filename)
            out_object.remove_null_normals()

        elif object_type.lower() == 'PolyLine'.lower():
            out_object = PolyLine()
            out_object.read(object_filename)

        elif object_type.lower() == 'PointCloud'.lower():
            out_object = PointCloud(points)
            out_object.read(object_filename)

        elif object_type.lower() == 'Landmark'.lower():
            out_object = Landmark(points)
            out_object.read(object_filename)

        elif object_type.lower() == 'Image'.lower():
            img_data, dimension, img_affine = read_image(object_filename)
            img_data, img_data_dtype = normalize_image_intensities(img_data) # rescaling between 0. and 1.
            out_object = Image(img_data, img_data_dtype, img_affine)

        else:
            raise TypeError('Unknown object type: ' + object_type)

        return out_object
