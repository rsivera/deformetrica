import warnings
import os

import numpy as np

# Mesh readers
from vtk import vtkPolyDataReader, vtkSTLReader
from vtk.util import numpy_support as nps


connectivity_names = {2: 'LINES', 3: 'POLYGONS'}
connectivity_degrees = {'LINES': 2, 'VERTICES': 2, 'POLYGONS': 3}


def get_lines(poly_data):
    '''
    lines from a polydata
    can read one long line or a list of segment (2 points each)
    '''

    nlines = poly_data.GetNumberOfLines()
    lines = nps.vtk_to_numpy(poly_data.GetLines().GetData()).reshape((nlines, -1))

    if lines.shape[1] == 3:
        return lines[:, 1:]
    elif nlines == 1:
        lines2 = np.zeros((lines.shape[1]-2, 2), dtype=lines.dtype)
        for i in range(1, lines.shape[1]-1):
            lines2[i-1, 0] = lines[0, i]
            lines2[i-1, 1] = lines[0, i+1]
        return lines2
    else:
        raise NotImplementedError("Trying to read file with several long lines {}".format(lines.shape))


def get_triangles(poly_data):
    ''' should check if only triangle, could use vtkTriangleFilter '''
    npolys = poly_data.GetNumberOfPolys()
    polygons = nps.vtk_to_numpy(poly_data.GetPolys().GetData()).reshape((-1, 4))[:, 1:]
    return polygons


def read_file(filename, dimension=3, extract_connectivity='auto'):
    """
    read VTK files based on the VTK library (available from conda).
    extract_connectivity in auto, none, line, poly
    """
    assert os.path.isfile(filename), 'File does not exist: %s' % filename

    # choose vtk reader depending on file extension
    if filename.find(".vtk") > 0:
        poly_data_reader = vtkPolyDataReader()
    elif filename.find(".stl") > 0:
        poly_data_reader = vtkSTLReader()
    else:
        raise RuntimeError("Unrecognized file extension: " + filename)

    poly_data_reader.SetFileName(filename)
    poly_data_reader.Update()
    poly_data = poly_data_reader.GetOutput()

    points = nps.vtk_to_numpy(poly_data.GetPoints().GetData()).astype('float64')

    if dimension == 2:
        points = points[:, :dimension]

    if extract_connectivity == 'auto':
        nlines = poly_data.GetNumberOfLines()
        npolys = poly_data.GetNumberOfPolys()
        if nlines == 0 and npolys == 0:
            extract_connectivity == 'none'
        elif npolys == 0 or dimension == 2:
            extract_connectivity == 'line'
        else:
            extract_connectivity == 'poly'

    if extract_connectivity == 'none':
        connectivity = None
    elif extract_connectivity == 'line':
        connectivity = get_lines(poly_data)
    else:
        connectivity = get_triangles(poly_data)
    return points, connectivity


def write_file(filename, points, connectivity=None):
    '''
    points and connectivity should be numpy array
    only write legacy vtkPolyData files
    '''

    if not filename.endswith('.vtk'):
        filename += '.vtk'

    with open(filename, 'w', encoding='utf-8') as f:
        s = '# vtk DataFile Version 3.0\nvtk output\nASCII\nDATASET POLYDATA\nPOINTS {} float\n'.format(len(points))
        f.write(s)

        for p in points:
            str_p = [str(elt) for elt in p]
            if len(p) == 2:
                str_p.append(str(0.))
            s = ' '.join(str_p) + '\n'
            f.write(s)

        if connectivity is not None:
            n, connec_degree = connectivity.shape
            s = connectivity_names[connec_degree] + ' {} {}\n'.format(n, n * (connec_degree+1))
            f.write(s)
            for face in connectivity:
                s = str(connec_degree) + ' ' + ' '.join([str(elt) for elt in face]) + '\n'
                f.write(s)
