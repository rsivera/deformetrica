import warnings
import os

# Image readers
import PIL.Image as pimg
import nibabel as nib
import numpy as np


def read_image(filename):

    if filename.find(".png") > 0:
        img_data = np.array(pimg.open(filename))
        img_affine = np.eye(dimension + 1)

        if dimension == 3 and img_data.shape[2] == 3:
            warnings.warn('Multi-channel images are not managed (yet). Defaulting to the first channel.')
            img_data = img_data[:, :, 0]

    elif filename.find(".npy") > 0:
        img_data = np.load(filename)
        img_affine = np.eye(dimension + 1)

    elif filename.find(".nii") > 0 or filename.find(".nii.gz") > 0:
        img = nib.load(filename)
        img_data = img.get_data()
        img_affine = img.affine

    else:
        raise TypeError('Unknown image extension for file: %s' % filename)

    dimension = len(img_data.shape)
    return img_data, dimension, img_affine
