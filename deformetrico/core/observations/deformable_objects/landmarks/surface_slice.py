import numpy as np
import torch

from .....core import default
from .....core.observations.deformable_objects.landmarks.landmark_normals import LandmarkNormals
from .....core.observations.deformable_objects.landmarks.surface_mesh import SurfaceMesh
from .....core.observations.deformable_objects.landmarks.poly_line import PolyLine
from .....support import utilities


class Plane():
    """
    A plane
    assume torch tensors on the right device
    """
    def __init__(self, normal, offset, sigma):
        assert (normal.size(-1) == 3)
        self.normal = normal
        self.offset = offset
        self.sigma = sigma
        self.fn = self.F(self.normal)
    def F(self, points):
        ''' points (k,3) return (k,1) '''
        if points.ndim == 1:
            return torch.dot(self.normal, points) + self.offset
        elif points.ndim == 2:
            return torch.mm(points, self.normal.view(3,1)) + self.offset
        else:
            raise NotImplementedError('plane function 3D reductions not implemented')
    def G(self, points):
        ''' points (k,3) return (k,1) '''
        return torch.exp(-self.F(points)**2 / (2*self.sigma**2)) / (self.sigma * np.sqrt(2*np.pi))

    def project(self, points):
        ''' points (k,3) return (k,3) '''
        return points - self.F(points).view((-1,1)) * self.normal.view((1,3)) / self.fn

class SurfaceSlice(LandmarkNormals):
    """
    Slice of a 3D Triangular mesh or planar curve
    """

    ############################################################################
    ### Constructor:
    ############################################################################

    def __init__(self, points=None, triangles=None, normal=None, offset=0, **kwargs):
        super().__init__(points, triangles, **kwargs)

        if normal is None:
            normal = [0, 0, 1]

        self.type = 'SurfaceSlice'
        self.plane = Plane(utilities.move_data(normal, dtype=default.tensor_scalar_type.dtype, device='cpu'), offset, 5.)

    ############################################################################
    ### Public methods:
    ############################################################################
    @staticmethod
    def _get_centers_and_normals(points, segments, subtype, plane,
                                 tensor_scalar_type=default.tensor_scalar_type,
                                 tensor_integer_type=default.tensor_integer_type,
                                 device='cpu'):

        if subtype == 'line':
            centers, normals = PolyLine._get_centers_and_normals(
                points, segments,
                tensor_integer_type=tensor_integer_type, device=device)

        elif subtype == 'surface':
            centers, normals = SurfaceMesh._get_centers_and_normals(
                points, segments,
                tensor_integer_type=tensor_integer_type, device=device)
            A = torch.ones((normals.shape[0], 1), dtype=tensor_scalar_type.dtype)
            z = torch.mm(A, plane.normal.view((1,3)))
            s = plane.G(centers)
            normals = torch.cross(normals, z, dim=1) * s

        else:
            raise ValueError('SurfaceSlice unknown subtype {}'.format(self.subtype))

        return centers, normals

    def get_centers_and_normals(self, points=None,
                                tensor_scalar_type=default.tensor_scalar_type,
                                tensor_integer_type=default.tensor_integer_type,
                                device='cpu'):
        """
        Given a new set of points, use the corresponding connectivity available in the polydata
        to compute the new normals, all in torch
        """
        if points is None:
            self.update_centers_and_normals()
            return (utilities.move_data(self.centers, dtype=tensor_scalar_type, device=device),
                    utilities.move_data(self.normals, dtype=tensor_scalar_type, device=device))
        else:
            return type(self)._get_centers_and_normals(
                points, torch.from_numpy(self.connectivity),
                self.subtype, self.plane,
                tensor_scalar_type=tensor_scalar_type, tensor_integer_type=tensor_integer_type, device=device)

    def update_centers_and_normals(self, is_modified=False):
        if is_modified or self.is_modified:
            self.centers, self.normals = type(self)._get_centers_and_normals(
                torch.from_numpy(self.points), torch.from_numpy(self.connectivity),
                self.subtype, self.plane)
            self.is_modified = False

    def read(self, fname):
        self.logger.debug('reading SurfaceSlice: {}'.format(fname))

        self.points = np.load(fname + '_points.npy').astype("float32")
        self.connectivity = np.load(fname + '_connect.npy').astype("int32")
        pplane = np.load(fname + '_plane.npy').astype("float32")

        if self.connectivity.ndim == 1 or self.connectivity.shape[1] == 1:    # line
            raise ValueError('SurfaceSlice do not read 1D connectivity for lines: {}'.format(self.connectivity.shape))
        elif self.connectivity.shape[1] == 2:    # line
            self.subtype = 'line'
        elif self.connectivity.shape[1] == 3:  # surface
            self.subtype = 'surface'
        else:
            raise ValueError('SurfaceSlice only work with triangle or segment connectivity: {}'.format(self.connectivity.shape))

        self.plane = Plane(utilities.move_data(pplane[:3], dtype=default.tensor_scalar_type.dtype, device='cpu'), pplane[3], pplane[4])

        self.norm = None
        self.is_modified = True

        _ = self.bounding_box # update
