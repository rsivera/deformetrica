from abc import abstractmethod

import numpy as np
import torch

from .....core import default
from .....core.observations.deformable_objects.landmarks.landmark import Landmark
from .....support import utilities


class LandmarkNormals(Landmark):
    """
    Landmarks with normals
    """

    ############################################################################
    ### Constructor:
    ############################################################################
    def __init__(self, points=None, triangles=None, **kwargs):
        super().__init__(points, triangles, **kwargs)

    ############################################################################
    ### Public methods:
    ############################################################################
    @staticmethod
    @abstractmethod
    def _get_centers_and_normals(points, triangles,
                                 tensor_scalar_type=default.tensor_scalar_type,
                                 tensor_integer_type=default.tensor_integer_type,
                                 device='cpu'):
        raise NotImplementedError


    def remove_null_normals(self):
        _, normals = self.get_centers_and_normals()
        triangles_to_keep = torch.nonzero(torch.norm(normals, 2, 1) != 0, as_tuple=False)

        if len(triangles_to_keep) < len(normals):
            self.logger.info(
                'I detected {} null area triangles, I am removing them'.format(len(normals) - len(triangles_to_keep)))
            new_connectivity = self.connectivity[triangles_to_keep.view(-1)]
            new_connectivity = np.copy(new_connectivity)
            self.connectivity = new_connectivity
            self.is_modified = True

    def get_centers_and_normals(self, points=None,
                                tensor_scalar_type=default.tensor_scalar_type,
                                tensor_integer_type=default.tensor_integer_type,
                                device='cpu'):
        """
        Given a new set of points, use the corresponding connectivity available in the polydata
        to compute the new normals, all in torch
        """
        if points is None:
            self.update_centers_and_normals()
            return (utilities.move_data(self.centers, dtype=tensor_scalar_type, device=device),
                    utilities.move_data(self.normals, dtype=tensor_scalar_type, device=device))
        else:
            return type(self)._get_centers_and_normals(
                points, torch.from_numpy(self.connectivity),
                tensor_scalar_type=tensor_scalar_type, tensor_integer_type=tensor_integer_type, device=device)

    def update_centers_and_normals(self, is_modified=False):
        if is_modified or self.is_modified:
            self.centers, self.normals = type(self)._get_centers_and_normals(
                torch.from_numpy(self.points), torch.from_numpy(self.connectivity))
            self.is_modified = False
