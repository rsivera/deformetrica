import os.path

import numpy as np
import torch

from .....in_out import vtkpolydata_reader_and_writer as vtk_io

class Landmark:
    """
    Landmarks (i.e. labelled point sets).
    The Landmark class represents a set of labelled points. This class assumes that the source and the target
    have the same number of points with a point-to-point correspondence.
    """

    ############################################################################
    ### Constructor:
    ############################################################################

    # Constructor.
    def __init__(self, points=None, connectivity=None, logger=None):

        self.type = 'Landmark'
        self.norm = None
        self.points = points
        self.connectivity = connectivity
        self.is_modified = True

        self._bounding_box = None

        if logger is None:
            from .....core import logger
            self.logger = logger
        else:
            self.logger = logger

    @property
    def dimension(self):
        if self.points is None:
            return 0
        elif self.points.shape[0] == 0:
            return 0
        else:
            return self.points.shape[1]

    @property
    def number_of_points(self):
        return len(self.points)

    @property
    def bounding_box(self):
        if self.is_modified:
            self._bounding_box = np.zeros((self.dimension, 2))
            for d in range(self.dimension):
                self._bounding_box[d, 0] = np.min(self.points[:, d])
                self._bounding_box[d, 1] = np.max(self.points[:, d])
        return self._bounding_box


    ############################################################################
    ### Encapsulation methods:
    ############################################################################



    def set_points(self, points):
        self.points = points
        self.is_modified = True

    def set_connectivity(self, connectivity):
        self.connectivity = connectivity
        self.is_modified = True

    def get_points(self):
        return self.points

    ############################################################################
    ### Public methods:
    ############################################################################


    def read(self, fname, connectivity='auto'):
        self.points, self.connectivity = vtk_io.read_file(fname, extract_connectivity=connectivity)
        self.norm = None
        self.is_modified = True
        _ = self.bounding_box # update

    def write(self, output_dir, fname, points=None):
        if points is None:
            points = self.points
        connectivity = self.connectivity

        if isinstance(points, torch.Tensor):
            points = points.detach().cpu().numpy()
        if isinstance(connectivity, torch.Tensor):
            connectivity = connectivity.detach().cpu().numpy()

        vtk_io.write_file(
            os.path.join(output_dir, fname),
            points, self.connectivity)
