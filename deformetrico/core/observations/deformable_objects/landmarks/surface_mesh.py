import numpy as np
import torch

from .....core import default
from .....core.observations.deformable_objects.landmarks.landmark_normals import LandmarkNormals
from .....support import utilities

from .....in_out import vtkpolydata_reader_and_writer as vtk_io


class SurfaceMesh(LandmarkNormals):
    """
    3D Triangular mesh.
    """

    ############################################################################
    ### Constructor:
    ############################################################################

    def __init__(self, points=None, triangles=None, **kwargs):
        super().__init__(points, triangles, **kwargs)
        self.type = 'SurfaceMesh'


    ############################################################################
    ### Public methods:
    ############################################################################
    @staticmethod
    def _get_centers_and_normals(points, triangles,
                                 tensor_scalar_type=default.tensor_scalar_type,
                                 tensor_integer_type=default.tensor_integer_type,
                                 device='cpu'):

        points = utilities.move_data(points, dtype=tensor_scalar_type, device=device)
        triangles = utilities.move_data(triangles, dtype=tensor_integer_type, device=device)

        a = points[triangles[:, 0]]
        b = points[triangles[:, 1]]
        c = points[triangles[:, 2]]
        centers = (a + b + c) / 3.
        normals = torch.cross(b - a, c - a) / 2

        assert torch.device(device) == centers.device == normals.device
        return centers, normals

    def read(self, fname):
        super().read(fname, 'poly')
