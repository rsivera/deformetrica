import torch

from .....core import default
from .....core.observations.deformable_objects.landmarks.landmark_normals import LandmarkNormals
from .....support import utilities


class PolyLine(LandmarkNormals):
    """
    Lines in 2D or 3D space
    """
    ####################################################################################################################
    ### Constructor:
    ####################################################################################################################

    def __init__(self, points=None, segments=None, **kwargs):
        super().__init__(points, segments, **kwargs)
        self.type = 'PolyLine'

    ####################################################################################################################
    ### Public methods:
    ####################################################################################################################

    @staticmethod
    def _get_centers_and_normals(points, segments,
                                 tensor_scalar_type=default.tensor_scalar_type,
                                 tensor_integer_type=default.tensor_integer_type,
                                 device='cpu'):

        points = utilities.move_data(points, dtype=tensor_scalar_type, device=device)
        segments = utilities.move_data(segments, dtype=tensor_integer_type, device=device)

        a = points[segments[:, 0]]
        b = points[segments[:, 1]]
        centers = (a + b) / 2.
        normals = b - a

        assert torch.device(device) == centers.device == normals.device
        return centers, normals

    def read(self, fname):
        super().read(fname, 'line')
